-module(sample_exam).
-compile(export_all).







%%%%%%%%%%%%%%%%%%%%%%%% Simple Task %%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Problem 3b
find(X,[])    -> false;
find(X,[H|T]) when X =:= H -> true;
find(X,[H|T]) when X =/= H -> find(X,T).

%% Problem 4A.
%% Given code
start() ->
	spawn(fun() -> init() end).

init() -> loop(1).

loop(N) ->
	receive
		{read,Pid} ->
			Pid ! {value,self(),N},
			loop(N);
		{write,New} ->
			Pid ! {wrote,New},
			loop(New)
		end.


read(ServerPid) ->
	ServerPid ! {read,self()},
	receive
		{value,ServerPid,N} -> 
			N 
	end.













%%%%%%%%%%%%%%%%%%%%%%%% Simple Task %%%%%%%%%%%%%%%%%%%%%%%%%%%