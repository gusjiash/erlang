
-module(temp).
-compile(export_all).

% -export([f2c/1,c2f/1,convert/1,head/1,second/1,add/2,factorial/1,area/3,
% 		area/1,speak/1,sum2/1,test/1,add2/2,one/0,two/0,dolphin1/0,dolphin2/0,
% 		dolphin3/0,fridge/1,store/2,store2/2,take/2,take2/2,start/1,important/0,
% 		normal/0,myproc/0,chain/1,filter/2,loop/1]). 
-record(state,{server,name="",to_go=0}).



f2c(Fahrenheit) -> (Fahrenheit - 32) * 5/9.
c2f(Celsius)    ->  (Celsius *9/5)+ 32.

convert({fahrenheit,Temp}) ->({celsius,f2c(Temp)});
convert({celsius,Temp}) ->({fahrenheit,c2f(Temp)}).

head([H|_]) -> H.

second([_,X|_]) -> X.

test(X) -> X+X.

add(X,Y)->
	X + Y.

factorial(N) when N > 0 -> N * factorial(N - 1);
factorial(0) -> 1.

area(Type, N, M) ->
	case Type of
		square -> N * N;
		circle ->  0.34 * N * N;
		triangle -> 0.5 * N * M
	end.

% crate a tuple in the function, only need to export 1 variable
area({square,N}) -> N * N;
area({circle,M}) -> 0.34 * M * M;
area({triangle,O,P}) -> 0.5 * O * P;
area(_) -> {error,invalidObject}.

speak(Animal) ->
	Talk = if
		(Animal == cat) -> miaomiao;
		(Animal == dog) -> roofroof;
		true -> no_animal
	end,
	io:format(" ~w says ~w ~n ", [Animal,Talk]).


% greet({male, Name}) -> io:format("Hello, Mr. ~s",[Name]);
% greet({female, Name}) -> io:format("Hello, Mrs. ~s",[Name]);
% greet({_, Name}) -> io:format("Hello, ~s",[Name]).
% greet(Gender) ->
% 	Check = if
% 		(Gender == male) -> "Mr.";
% 		(Gender == female) -> "Mrs.";
% 		true -> "_"
% 	end,
% 	io:format("Hello, ~w ~w ~n", [Check,Gender]).




% Another way to sum the list. An more efficiency way
sum2(L) -> sum2(L,0).
sum2([], N) -> N;
sum2([X|XS], N) -> sum2(XS, N + X).


% course exercise for patter match:

% p1(false, _) -> answer1;
% p1(_, {blue, P}) -> answer2;
% p1([X|Xs], {Y,X}) -> [Y|Xs];
% p1(_,_) -> nothing_matched.

% temp:p1(true,{blue,[1,2,3]}).
% 	answer2
% temp:p1([a,b,c],{false,[b,c]}).
% 	nothing_matched
% temp:p1([false], {green,false}).
%  	[green]
% temp:p1(false,{blue,green}).
% 	answer1,
% 	the second answer also match the forth question, but when the first one matched, erlang will stack at the first answer.



% Higher Order Function Exercise:

one() -> 1.
two() -> 2.
add2(X,Y) ->X() + Y().

% % increment([]) -> [];
% increment([H|T]) -> [H+1|increment(T)].

% % decrement([]) -> [];
% decrement([H|T]) -> [H-1|decrement(T)].



%%%%%%%%%%%%%%%%%%%%%%%% chapter 10 %%%%%%%%%%%%%%%%%%%%%%%%%%%
dolphin1() ->
	receive
		do_a_flip ->
			io:format("How about no?~n");
		fish	  ->
			io:format("So long and thanks for all the fish!~n");
		 _ 		  ->
		 	io:format("Heh, we're smarter then you humans.~n")
 	end.

dolphin2() ->
	receive
		{From, do_a_flip} ->
			From ! "How about no?";
		{From, fish}      ->
			From ! "So long and thanks for all the fish!" ;
		_                 ->
			io:format("Heh, we're smarter then you humans.~n")
	end.

dolphin3() ->
	receive
		{From, do_a_flip} ->
			From ! "How about no?",
			dolphin3();
		{From, fish}      ->
			From ! "So long and thanks for all the fish!";
		_                 ->
			io:format("Hey, we're smarter then you humans.~n"),
			dolphin3()
		end.



%%%%%%%%%%%%%%%%%%%%%%%% chapter 11 %%%%%%%%%%%%%%%%%%%%%%%%%%%


fridge(FoodList) ->
	receive
		{From,{store,Food}} ->
			From ! {self(), ok},
			fridge([Food|FoodList]);
		{From,{take, Food}} ->
			case lists:member(Food,FoodList) of
				true ->
					From ! {self(), {ok,Food}},
					fridge(lists:delete(Food,FoodList));
				false ->
					From ! {self(), not_found},
					fridge(FoodList)
			end;
		terminate -> 
			ok
		end.

%%% Store food
		% 5> Pid = spawn(temp,fridge,[[baking_sode]]).
		% <0.53.0>
		% 6> Pid ! {self(), {store,milk}}.
		% {<0.32.0>,{store,milk}}
		% 7> flush().
		% Shell got {<0.53.0>,ok}
		% ok

%%% Store food & Get food.
		% 8> Pid ! {self(),{store, bacon}}.
		% {<0.32.0>,{store,bacon}}
		% 9> Pid ! {self(),{store, bacon}}.
		% {<0.32.0>,{store,bacon}}
		% 10> flush().
		% Shell got {<0.53.0>,ok}
		% Shell got {<0.53.0>,ok}
		% ok
		% 11> Pid ! {self(),{take,turkey}}.
		% {<0.32.0>,{take,turkey}}
		% 12> flush().
		% Shell got {<0.53.0>,not_found}
		% ok
		% 13> Pid ! {self(),{take, bacon}}.
		% {<0.32.0>,{take,bacon}}
		% 14> flush().
		% Shell got {<0.53.0>,{ok,bacon}}
		% ok


%%% abstract message away to keep them secret.

store(Pid, Food) ->
	Pid ! {self(), {store,Food}},
	receive
		{Pid,Msg} ->Msg
	end.
take(Pid, Food)  ->
	Pid ! {self(), {take,Food}},
	receive
		{Pid,Msg} ->Msg
	end.

start(FoodList) ->
	spawn(?MODULE,fridge,[FoodList]).


%%% with after Delay function:
	% receive
	% 	Match -> Expression1
	% after 
	% 	Delay -> Expression2
	% end.

store2(Pid,Food) ->
	Pid ! {self(), {store,Food}},
	receive
		{Pid,Msg} ->Msg
	after 3000 ->
		timeout
	end.

take2(Pid,Food) ->
	Pid ! {self(), {take, Food}},
	receive
		{Pid,Msg} -> Msg
	after 3000 ->
		timeout
	end.

important() ->
	receive
		{Priority,Message} when Priority > 10 ->
			[Message|important()]
	after 0 ->
		normal()
	end.

normal() ->
	receive
		{_,Message} ->
			[Message|normal()]
	after 0 ->
		[]
	end.
	% 9> self() ! {15,high}, self() ! {7,low}, self() ! {11,high}.
	% {11,high}
	% 10> temp:important().
	% [high,high|normal]
	% 11> 

%%%%%%%%%%%%%%%%%%%%%%%% chapter 11 %%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%% chapter 12 %%%%%%%%%%%%%%%%%%%%%%%%%%%

myproc() ->
	timer:sleep(5000),
	exit(reason).

	% 26> spawn(fun temp:myproc/0).
	% 	<0.168.0>
	% 27> self().
	% 	<0.126.0>
	% 28> link(spawn(fun linkmon:myproc/0)).
	% 	true
	% 	** exception error: undefined function linkmon:myproc/0
	% 29> 
	% 	=ERROR REPORT==== 7-Oct-2014::08:45:47 ===
	% 	Error in process <0.171.0> with exit value: {undef,[{linkmon,myproc,[],[]}]}

chain(0) ->
	receive
		_   -> ok
	after 2000 
			-> exit("chain dies here")
	end;
chain(N) ->
	Pid = spawn(fun() -> chain(N-1) end),
	link(Pid),
	receive
		_	-> ok
	end.

% 41> link(spawn(temp,chain,[3])).             
% true
% ** exception error: "chain dies here"

% Using System Process(convert exit signal to regular messages)
	% 44> process_flag(trap_exit,true).
	% 	false
	% 45> spawn_link(fun() -> temp:chain(3) end).
	% 	<0.234.0>
	% 46> receive X -> X end.
	% 	{'EXIT',<0.234.0>,"chain dies here"}
	% 47> process_flag(trap_exit,true).          
	% 	true

%%%%%%%%%%%%%%%%%%%%%%%% chapter 12 %%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%% Optional Exercise %%%%%%%%%%%%%%%%%%%%%%%%%%%
%Question 2.5
%  filter([1,2,3,4,5],3) -> [1,2,3]
filter(_,_) -> ok.



%%%%%%%%%%%%%%%%%%%%%%%% Optional Exercise %%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%% chapter 13 %%%%%%%%%%%%%%%%%%%%%%%%%%%
loop(S = #state{server=Server}) ->
	receive
		{Server,Ref,cancel} ->
			Server ! {Ref,ok}
		after S#state.to_go*1000 ->
			Server ! {done, S#state.name}
		end.

		% 5> c(temp).
		% 	{ok,temp}
		% 7> rr(temp,state).
		% 	[state]
		% 8> spawn(temp,loop,[#state{server=self(),name="test",to_go=5}]).
		% 	<0.60.0>
		% 9> flush().
		% 	ok
		% 10> flush().
		% 	Shell got {done,"test"}
		% 	ok
		% 25> Pid = spawn(temp,loop,[#state{server=self(),name="test",to_go=500}]).
		% 	<0.80.0>
		% 26> ReplyRef = make_ref().
		% 	#Ref<0.0.0.692>
		% 27> Pid ! {self(), ReplyRef, cancel}.
		% 	{<0.32.0>,#Ref<0.0.0.692>,cancel}
		% 28> flush().
		% 	Shell got {#Ref<0.0.0.692>,ok}
		% 	ok

%% Because Erlang is limited to about 49 days(49*24*60*60*1000) in
%% millisecond, the following function is used.

normalize(N) ->
	Limit = 49*24*60*60,
	[N rem Limit | lists:duplicate(N div Limit,Limit)].

%% update loop function in order to go around the 49 days
%% limit on timeout.


loop1(S = #state{server=Server,to_go=[T|Next]}) ->
	receive
		{Server, Ref, cancel} ->
			Server ! {Ref,ok}
	after T*1000 ->
		if  Next =:= [] ->
				Server ! {done,S#state.name};
			Next =/= [] ->
				loop(S#state{to_go=Next})
			end
	end.
%% Add an interface
%% init function will handle all initialization of data
%% required for the loop function to work well.

start(EventName,Delay) ->
	spawn(?MODULE, init, [self(),EventName,Delay]).
start_link(EventName,Delay) ->
	spawn_link(?MODULE,init,[self(),EventName,Delay]).
init(Server,EventName,Delay) ->
	loop(#state{server = Server, name = EventName, 
				to_go = normalize(Delay)}).


%%%%%%%%%%%%%%%%%%%%%%%% chapter 13 %%%%%%%%%%%%%%%%%%%%%%%%%%%

































