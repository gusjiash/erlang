open erlang:
[11:11:51] user :: dhcp-079215  ➜  ~ » . /usr/local/Cellar/erlang/17.0/activate
[11:12:08] user :: dhcp-079215  ➜  ~ » erl
Erlang/OTP 17 [erts-6.0] [source] [64-bit] [smp:4:4] [async-threads:10] [hipe] [kernel-poll:false]

Eshell V6.0  (abort with ^G)




load mode:
c(model name).
  find model file: go to the document—vim
q().
  quite
CTRL+G 
 +h . help
 42> CTR+G     
User switch command
 --> i
 --> s
 --> c
Eshell V6.0  (abort with ^G)
1> 
atom,which you see is what you get+’UPCASE’ can also be atom
UP-CASE word can be veritable

touple:
T= {adam,21,2.4,{jum,23}}.
element (1,T)—————>adam.
T2 = setelement(2,T,99)————>{adam,99,2.4,{jum,23}}
tuple_size(T).————>4
*PreciseTemperature = {celsius, 23.213} --> {celsius,23.213}
{kelvin, T} = PreciseTemperature -->  ** exception error: no match of right hand side value {celsius,23.213}
explain: celsius atom is not as identical to the keivin atom when comparing them
*'tagged tuple': A tupe contains an atom with one element.
*Any elemengt of a tuple can be of any type, even another tuple:
	{point,{X,Y}}. -> {point,{4,5}}
 
list:
L1 = [monday, tuesday,wednesday].
L2 = [1,[2,3],[4,5]].
get head and tail:
[H|T]=L2————>[1,[2,3],[4,5]].
H.———>1
T.—————>[2,3],[4,5]
length(L1).——>3

check banded:
b()———>H = 1
	   T=[[2,3],[4,5]]
       L1=[monday,tuesday,wednesday]
forget all the variable banded:f().

L1 = [1,2,3,4,5,6,7,8,9,10]
length(L1)-----> 10
lists:max(L1)--->10
lists:reverse(L1)--->[10,9,,8,7,6,5,4,3,2,1].
lists:sort(L1).
lists:sum(L1).
lists:delete(3,L1)--->[1,2,4,5,6,7,8,9,10].
*get head: hd ([1,2,3,4]). ---> 1
 get tail: tl ([1,2,3,4]). ---> [2,3,4]

*add head: List = [2,3,4]. ---> [2,3,4]
		   NewList = [1|List]. ---> [1,2,3,4]

*[Head|Tail] = NewList. ---> [1,2,3,4]
  Head. ---> 1
  Tail. ---> [2,3,4]
  [NewHead|NewTail] = Tail. ---> [2,3,4]
  NewHead. ---> 2






% write an easy module.
% the module file save in erlang folder on the desk as easy.erlang
//how to compile:
c(easy).
easy:add(1.2).
-------->3



% listlist
%crate a list:
[{X,A}||{A,X} <- [{a,1},{b,5}]].
filter:
[X||{a,X} <-[{a,1},{b,5}]]. 
----->[1]

[{X,Y}||X <- [1,2,3,4,5],Y <- [0,X]].
[{1,0},
 {1,x},
 {2,0},
 {2,x},
 {3,0},
 {3,x},
 {4,0},
 {4,x},
 {5,0},
 {5,x}]

 [X || {A,X} <- [{a,1},{b,5}]].
 ->[1,5]

 [X || {a,X} <- [{a,1},{b,5}]].
->[1]


% Guards
in function(when,,buildin function); if statement; case statement,
BIF(build in function):hd,tl,length,
*errror: when evaluation an arithmetic expression in function error: f1/0
		no function clause matching error: g2(3)
 catch(error:f1()). will still get result with an error happengin in the process.
 	OR
 try f1(X) 
 	catch 
 		error:Err -> {error_caught,Err}
	end.
(Read Errors and Exceptions)



map(_F, [])-> [];
map(F, [X|Xs]) -> [F(X) | map( F, Xs)].

add2E(X) -> X+2.
% padd the add2E as argument in the map function:
map(fun add2E/1), [2,3,5]) ->[4,5,7].

% OR
mul2E(x) -> X*2.
map(fun mul2E/1, [2,3,5]) -> [4,6,10].


lists:ma
lists:map(fun guards:double/1,[1,2,3,4]).

% another way to do the same thing as the prievew one.
*map(fun(X) -> X+2 END, [2,3,5] -> [4,5,7].


% if statement


% Recursion





%%%%%%%%%%%%%%%%%%%%%%%%%% 2014-09-30 Lecture %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Spawn
  take function as argument;
  take three arguemnt(modul name, function name, list of argument).

  example:
  30> F = fun() -> io:format("Hello ~n") end, G = fun() ->  io:format("Hello again ~n") end, spawn(F), spawn(G), ok.
  Hello 
  Hello again 
  ok

  three process at the same time, they are all independed, the fast process will print out the answer first.
  So the "Hello again" might print out first.

  process id for the current process:
  31> self().
  <0.66.0>

% Send message
  self() ! hello ----- left side takes a pid, right side takes any Erlang term.
                       message is any erlang value 

  %Ex:
  self() ! self() ! double   ===   self()! (self()!double)
    fist step self() ! double
    second step double   ----- result.
  33> self().
  <0.66.0>
  34> flush().     %find the message in the self
  Shell got message
  ok

  35> self() ! message2.
  message2
  36> flush().
  Shell got message2
  ok

% I function find out all the process is running.
  i().
  Pid                   Initial Call                          Heap     Reds Msgs
  Registered            Current Function                     Stack              
  <0.0.0>               otp_ring0:start/2                      987     5069    0
  init                  init:loop/1                              2              
  <0.3.0>               erlang:apply/2                        6772   704159    0
  erl_prim_loader       erl_prim_loader:loop/3                   6              

%Something don't understand
  40> P = spawn(fun () -> timer:sleep(6000) end).
  <0.125.0>
  42> erlang: process_info(P).



% CHECK OUT "pman:start()."

% Message receive statement.
  Will look through all the message in mail box.


  1> F = fun() -> receive X -> io:format("Received ~p~n", [X]) end end,
  1> P = spawn(F),
  1> G = fun() -> P!message end,
  1> spawn(G), 
  1> ok.
  Received message
  ok
  First: till ok
  Second: X receive message 
  Third: P! message.
  Forth: shell finished runs ok, or the P!message could excute 




    


  F, G are function defined locally.
  spawn the process, receive(with nothing in mail box), crate another process, spawn function, 
  send message to the first mail box, receive the message and print out.
  "ok" will print out


  In the receive X is a pattern, can be match anything. then the value of X will become the message we send to it.

%%%% spawn
  SEND MESSAGE TO SHELL
  self been called the newly process.
  %%%% S = SELF.
  send message to PID, flush will get message been send.
  here the self been called in the current process.

%%% Monitors
  simmiler to links, But  it is one directionar, but Link work on both way.
  One process can monitor another process multiple times.

  monitor(process,P)  ---> returns refers
  demonitor(Ref)
  spawn_monitor(F)

%%% Error handling is part of API
  spawn_link -> cancle process.


%%%%%%%%%%%%%%%%%%%%%%%%%% 2014-09-30 Lecture %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%% Course Book Chapter 10 %%%%%%%%%%%%%%%%%%%%%%%%%%

self().         --- check current process identifier
exit(self()).   --- restart the process 
flush().        --- see the content of the current mailbox.
18> self() ! hello.
  hello
19> self()!self()!double.
  double
20> flush().
  Shell got hello
  Shell got double
  Shell got double
  ok

5> G = fun(X) -> timer:sleep(10), io:format("~p~n",[X]) end.
  #Fun<erl_eval.6.106461118>
6> [spawn(fun() -> G(X) end) || X <- lists:seq(1,10)].
  [<0.44.0>,<0.45.0>,<0.46.0>,<0.47.0>,<0.48.0>,<0.49.0>,
   <0.50.0>,<0.51.0>,<0.52.0>,<0.53.0>]
  10 
  9  
  4  
  8  
  3  
  7  
  2  
  1  
  6  
  5  

 % Reason: process are running at the same time, the ordering of events isn't 
 %         guaranteed anymore.

  Parallelism: having actors running at exactly the same time.
  Concurrency: refers to having many actors running independently but not 
               necesssariy all the same time.



  spawn_link(F)
  link(P)
  exit(P,Reason) Reason -> kill.
    %when kill on link process, the relative linked process will also be killed.
%Trapping:
  process_flat(trap_exit,true)  : when other likn get killed , this process wont 
                                  get killed automatically.

%%%%%%%%%%%%%%%%%%%%%%%%%% Course Book Chapter 10 %%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%% Course Book Chapter 11 %%%%%%%%%%%%%%%%%%%%%%%%%%
 *when the function is waiting forever--deadlock:
  receive
      Match -> Expression1
  after 
      Delay -> Expression2  %change Delay to number(milliseconds)
  end.
 this will not tell us how to deal with the fact process take too long,
  and the message might come back later in unexpected way

*exit Control+G 
  User switch command --> i
                      --> s
                      --> c

* Selective Receives
    Erlang’s “flushing” concept makes it possible to implement a 
    selective receive, which can give a priority to the messages 
    you receive by nesting calls




%%%%%%%%%%%%%%%%%%%%%%%%%% Course Book Chapter 11 %%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%% Map reducing %%%%%%%%%%%%%%%%%%%%%%%%%%

  NoSQL: not only SQL.


  Consistency: different clients connect different notds in the same database,
               when nods are not connect to each other, when one user change 
               things in one node, the information wont transfer to other node.
      Option 1: One big RDBMS, use one single big database, could never die,
                pay more money to keep it and to handle database.
      Option 2: one Master one slave, client connect both server. when read, cliend 
                get information from both; but when write on Slave, Slave should
                ask Master first---slow writes. When master died, can still read
                from Slave.
      Option 3: Sharding: have number of node, each node is the master of the partical of
                itself, if one node goes down, part of the data is unavailable; code 
                a lot logical manually.
  Amazion New Rules:
    Single-key 
    Weaker consistency: no need to have node to connect to every other nodes.

    





%%%%%%%%%%%%%%%%%%%%%%%%%% Map reducing %%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%% Lecture %%%%%%%%%%%%%%%%%%%%%%%%%%

Examle, again:
ex1:
    f(x) -> ..........
    ......
    pmap(fun f/1,[1,2,3]),
ex2:
    F = fun(x) -> .....end,
ex3:
    F = fun f/1,

*get the result in the right order with a pid in paramater.
  used by tag F(X) with self().
  (the third page example is better then the second page)

* make_ref()  --- crate a uniqe id 


Supervisior THE MOST IMPORTANT FEATURE IN ERLANG:
init(_Args) ->
  {ok, {{one_for_one,3,60}}}--- supervisior one to one,3times,60second.






%%%%%%%%%%%%%%%%%%%%%%%%%% Lecture %%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%% Chapter 14 %%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%% Chapter 14 %%%%%%%%%%%%%%%%%%%%%%%%%%






















































