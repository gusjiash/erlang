-module(seq_erlang).

%-include_lib("eqc/include/eqc.hrl").

-compile(export_all).
-export([sum/1, sum_interval/2, interval/2,
	 sum_interval2/2, adj_duplicates/1,
	 even_print/1, even_odd/1, even_print2/1,
	 normalize/1, normalize2/1,
         digitize/1, is_happy/1, all_happy/2,
         expr_eval/1, expr_print/1
        ]).

% 1. Basic functions, lists and tuples

sum(1) -> 1;
sum(N) when N > 1 -> N + sum(N - 1).


sum_interval(_N, _M) -> ok.


interval(_N, _M) -> ok.


sum_interval2(_N, _M) -> ok.


adj_duplicates(_L) -> ok.


even_print(_L) -> ok.


even_odd(_N) -> ok.


even_print2(_L) -> ok.


normalize(_L) -> ok.


normalize2(_L) -> ok.


% 2. Digitify a number 

digitize(_N) -> ok.


% 3. Happy numbers

is_happy(_N) -> ok.


all_happy(_N, _M) -> ok.


% 4. Expressions

expr_eval(_Expr) -> ok.


expr_print(_Expr) -> ok.

