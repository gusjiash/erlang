%%% Name            : Shan Jiang
%%% Personal number : 19890615-7923
%%% E-mail          : gusjiash@gmail.com

-module(dist_erlang).

%% DO NOT CHANGE THE EXPORT STATEMENT!
-export([start/0, init/0, stop/0, store/2, fetch/1, flush/0,
         task/1, dist_task/1, pmap/2, faulty_task/1
        ]).
%% End of no-change area

%%%%%%%%%%%%%%
%% Register processID inside of sts(a given name)
start() ->
  case whereis(sts) of
      undefined ->
          Pid = spawn(dist_erlang,init,[]),
          register(sts,Pid),
          {ok,Pid};
      Pid ->
          {ok,Pid}
  end.

%% Stop will only be process when there is at least one process in working
stop() ->
  case whereis(sts) of
      undefined ->
          already_stopped;
      Pid ->
      exit(Pid,stopp),
      stopped
    end.
%% store/2 returns ok and the old value (or no_value,
%% if the key has not been used before!).



%% Store/Fetch/Flush are the function on the client said, 
%% they will send input value and sand by process register name
%% to server

store(Key,Value) ->
      sts ! {self(),{store,Key,Value}},
      receive
        {Msg} ->
          Msg
      after 1000 ->
        timeout      
    end.

fetch(Key) -> 
      sts ! {self(),{fetch,Key}},
        receive
          {Msg} ->
            Msg
        after 1000 ->
          timeout        
      end.

flush() ->
 sts ! {self(),{flush}},
        receive
          {Msg} ->
            Msg
        after 1000 ->
          timeout
        
      end.

init() -> loop([]).


%% Main function on server
loop(Db) -> 
    receive

%% When receive Store, the server will fist check if the key inside this
%% Process is existed or not, if it is, return old value, if not, return no_value
%% then delete the old value by BIF lists:keydelete/3 and replace the new DB
      {From,{store,Key,Value}} ->
        Key_tuple = lists:keyfind({From,Key},1,Db),
        case Key_tuple of
          false->
            From!{{ok,no_value}};
          _->
            Old_value=element(2,Key_tuple),
            From!{{ok,Old_value}}         
        end,
        Delete_List=lists:keydelete({From,Key},1,Db),
        loop([{{From,Key},Value}|Delete_List]);
%% Same as the store function in server, but not store any data,
%% Only use BIF lists:keyfind/3 go check if this key is existed in this process
      {From,{fetch,Key}} ->
        Key_tuple = lists:keyfind({From,Key},1,Db),
        case Key_tuple of
          false->
            From!{{error,not_found}};
          _->
            Old_value=element(2,Key_tuple),
            From!{{ok,Old_value}}
        end,
        loop(Db);
 %% Used list comprehension to return the tuple inside database,which the 
 %% Key is not the one send by cliend. then crate new Database       
      {From,{flush}} ->
        New_Db = [T||T<-Db,element(1,element(1,T)) =/= From],
        From!{{ok,flushed}},   
        loop(New_Db)
    end.


%%
%% Problem 2
%%

%% Do not change the following two functions!
task(N) 
    when N < 0; 
    N > 100 ->
    exit(parameter_out_of_range);
task(N) ->
    timer:sleep(N * 2),
    256 + 17 *((N rem 13) + 3).

faulty_task(N) 
    when N < 0; 
    N > 100 ->
    exit(parameter_out_of_range);
faulty_task(N) ->
    timer:sleep(N * 2),
    {_,_,X} = now(),
    case X < 100 of
        false ->
            256 + 17 *((N rem 13) + 3);
        true  ->
            throw(unexpected_error)
    end.
%% End of no-change area

%% Crate a process for every element from input list by using list comprehension
%% and send by S to next step.then received all the result by the same sequence of 
%% PIDs
dist_task(Data) ->
    S = self(),
    Pids = [spawn(fun()->S!{self(),task(X)}end)||X<-Data],
           [receive {Pid,Result}->Result end||Pid<-Pids].
%% same as last one, but also catch all the throw from pre-function
pmap(F,Data) ->
    S = self(),
    R = make_ref(),
    Pids = [spawn(fun()->S!{R,self(),catch F(X)}end)||X<-Data],
           [receive{R,Pid,Result}->Result end||Pid<-Pids].
  
    











