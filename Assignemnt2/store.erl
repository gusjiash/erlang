2> P = {person, "Hernan", "Garcia", "@theprogrammer"}.
3> {person, Fname, Lname, Handler} = P.
4> Fname.
"Hernan"
5> Handler.
"@theprogrammer"
1> rd(person, {first = "", last = "", handler = "@"}).
person
2> P = #person{first = "Hernan"}.
#person{first = "Hernan",last = [],handler = "@"}
3> Fname = P#person.first.
4> Fname.
"Hernan"

1> P = #person{age = 25}.
* 1: field age undefined in record person

1> is_record(P, person).
true
2> A = "string".
3> is_record(A, person).
false

1> P#person{first = "Diego2"}.
#person{first = "Diego2",last = [],handler = "@"}
2> P.
#person{first = "Hernan",last = [],handler = "@"}
1> P2 = P#person{first= "Diego"}.
2> P2.
#person{first = "Diego",last = [],handler = "@"}
3> P.
#person{first = "Hernan",last = [],handler = "@"}

1> #person{first = Name} = P.
2> Name.
"Hernan"

1> rd(actor, {name="", gender=""}).
2> rd(movie, {title="", mainactor=actor}).
2> M = #movie{title = "The movie", mainactor = #actor{name = "Diva", gender = "F"}}.
3> M#movie.mainactor#actor.name.
"Diva"