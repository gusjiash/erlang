-module(basicserver).

-export([start/1, init/1, plus/2, get/1, stop/1]).

start(InitialState) ->
  spawn(basicserver, init, [InitialState]).

init(InitialState) ->
  loop(InitialState).

loop(M) ->
  receive
    {plus, N} ->
      loop(N + M);
    {get, P, Ref} ->
      P ! {get_reply, Ref, M},
      loop(M);
    stop -> ok
  end.

plus(S, N) ->
  S ! {plus, N},
  ok.

get(S) ->
  Ref = make_ref(),
  S ! {get, self(), Ref},
  receive
    {get_reply, Ref, M} -> M
  end.

stop(S) ->
  S ! stop,
  ok.
