-module(count).

-export([count_sum/3, sum/1]).

count_sum(N,N,I) -> I;
count_sum(N,M,I) ->
	I2 = N + I,
	count_sum(N+1,M,I2).

sum([]) -> 0;
sum([X|Xs]) -> X + sum(Xs).

