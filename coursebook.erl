-module(coursebook).
-export([add/2, hello/0, greet_and_add_two/1,greet/2,head/1,second/1,
		same/2,valid_time/1,old_enough/1,right_age/1]).

add(A,B) ->
	A + B.

hello() ->
	io:format("Hello, world! ~n").

greet_and_add_two(X) ->
	hello(),
	add(X,2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
greet(male,Name) ->
	io:format("Hello, Mr. ~s!", [Name]);
greet(female,Name) ->
	io:format("Hello, Mrs. ~s!", [Name]);
greet(_,Name) ->
	io:format("Hello, Mr. ~s!", [Name]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

head([H|_]) -> H.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

second([_,X|_]) ->X.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

same(X,X) -> true;
same(_,_) -> false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

valid_time({Date = {Y,M,D}, Time = {H, Min, S}}) ->
	io:format("The Date tuple (~p) says today is: ~p/~p/~p,~n", [Date,Y,M,D]),
	io:format("The time tuple (~p) indictes: ~p~p~p.~n",[Time,H,Min,S]);
valid_time(_) ->
	io:format("Stop feeding me wrong data! ~n").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

old_enough(X) when X >= 16 -> true;
old_enough(X) 			   -> false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%"," means and also
right_age(X) when X >= 16 , X =< 106 -> true;
right_age(X)                         -> false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%";" means or else
wrong_age(X) when X < 16 ; X > 106 -> true;
wrong_age(X)                       -> false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%














