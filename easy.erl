-module(easy).
-compile(export_all).

%%  Basic Concurrency
say_something(_,0) ->
	io:format("Done ~n");
say_something(Value,Times) ->
	io:format("~s ~n",[Value]),
	say_something(Value,Times-1).

start_concurrency(Value1,Value2) ->
	spawn(easy,say_something,[Value1,3]),
	spawn(easy,say_something,[Value2,3]).
%%========================================================
%%========================================================
%%  6> easy:start_concurrency("nihao","wojiaotoutou").
%%	nihao 											  
%%	wojiaotoutou 									  
%%	nihao 										      
%%	wojiaotoutou 								      
%%	<0.51.0>										  
%%	nihao 											  
%%	wojiaotoutou 									  
%%	Done 											  	
%%	Done 										      
%%========================================================
%%========================================================


%%  Send and Receive Messaage
messageRec() ->
	receive
		{factorial,Int} ->
			io:format("Factorial for ~p is ~p ~n", [Int, factorial(Int, 1)]),
			messageRec();
		{factorialRecorder,Int} ->
			{ok,IoDevice} = file:open("/Users/user/Dropbox/erlang/erlangCourse/ConC.dat",write),
			factorialRecorder(Int,1,IoDevice),
			io:format("Factorial Recorder Done. ~n",[]),
			file:close(IoDevice),
			messageRec;
		_->
			io:format("Invalid Match for ~p~n", [other]),
			messageRec
	end.

factorial(Int,Acc) when Int >0 ->
	factorial(Int-1,Acc*Int);
factorial(0,Acc) ->
	Acc.

factorialRecorder(Int,Acc,IoDevice) when Int >0 ->
	io:format(IoDevice,"Current Factorial Log: ~p~n",[Acc]),
	factorialRecorder(Int-1,Acc*Int,IoDevice);
factorialRecorder(0,Acc,IoDevice) ->
	io:format(IoDevice,"Factorial Results:~p~n",[Acc]).


%%========================================================
%%========================================================
% 6> Pid = spawn(fun easy:messageRec/0).
% <0.56.0>
% 7> Pid ! {factorial,10}.
% Factorial for 10 is 3628800 
% {factorial,10}
% 8> Pid ! {factorialRecord,10}.
% Invalid Match for other
% {factorialRecord,10}
% 9> Pid ! {factorialRecorder,10}.
% {factorialRecorder,10}
% 10> Pid ! {factorialRecord,10000000000}.
% {factorialRecord,10000000000}
% 11> I = 12.
% 12										      
%%========================================================
%%========================================================

%% Node Creation

























