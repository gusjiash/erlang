-module(binding).

-export([f/1,g/1,h/1,k/0,l/0]).

% This module demonstrates surprising cases for variable binding in Erlang.

% When all branches of a case expression bind the same variable, the variable
% binding 'leaks' out of the case expression, and is in scope in the statements
% that come after it. The same behaviour concerns if and receive statements.
% Ex.:
% 1> binding:f({a, 3}).
f(A) ->
	case A of
		{a, Y} -> ok;
		{c, Y} -> ok
	end,
	io:format("Y = ~p~n", [Y]),
	ok.

% Compiling this function will yield a compiler warning due to the fact that
% a binding that got out of the case expression is used in a pattern after it.
% The assignment will crash if Y is not 4. Ex.:
% 1> binding:g({a, abc}).
g(A) ->
	case A of
		{a, Y} -> ok;
		{c, Y} -> ok
	end,
	Y = 4,
	ok.


% The compiler warning can be avioided using this workaround.
% See
% http://erlang.org/pipermail/erlang-questions/2004-April/012101.html
h(A) ->
	Y = case A of
		{a, X} -> X;
		{c, Z} -> Z
	end,
	Y = 4,
	ok.

k() ->
	P = a,
	% Despite that P is bound, the binding is 'forgotten'
	% inside of the list comprehension. This triggers
	% two compiler warnings about unused P, and
	% shadowing warning.
	[ X || {P, X} <- [{a, 1}, {b, 2}] ].

l() ->
	P = a,
	% To use the value bound to P for filtering we need
	% to use a boolean guard instead of pattern matching
	[ X || {Q, X} <- [{a, 1}, {b, 2}], Q =:= P ].
