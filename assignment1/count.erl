-module(count).

-export([count_sum/2,sum/1]).

% Count the sum of numbers from N to M (not including M)
count_sum(N,N,I) -> I;
count_sum(N,M,I) ->
	I2 = N + I,
	count_sum(N+1,M,I2).

count_sum(N,M) -> count_sum(N,M,0).


% count_sum(0,3,0) -> count_sum(1,3,0) -> count_sum(2,3,1) -> ...

sum([]) -> 0;
sum([X|Xs]) -> X + sum(Xs).
% sum(Xs) 
