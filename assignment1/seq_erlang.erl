-module(seq_erlang).

-compile(export_all).
-export([sum/1, sum_interval/2, interval/2,
	 sum_interval2/2, adj_duplicates/1,
	 even_print/1, even_odd/1, even_print2/1,max_check/1,
	 normalize/1, normalize2/1,
         digitize/1, is_happy/1, all_happy/2,
         expr_eval/1, expr_print/1
        ]).


% Get the sum from all the element in the list:
% Solution: Put the head of the list outside, then repeat to the same thing with reaming tail, 
%	everything get outside the list add together
% input: list
% output: integer
sum([]    ) -> 0;
sum([N|Ns]) -> N + sum(Ns).



% Get the sum from all the number between input number
% Solution: When the second input number is less then the first one, output zero; Other way, 
%	using the same solution as the upper one until M is less then N
% input: two integer
% output: integer
sum_interval(_N, _M) when _M < _N -> 0;
sum_interval(_N, _M)			  -> _N + sum_interval(_N+1,_M).

% Print out all the number between two input number.
% Solution: Put the first element as a list outside, then use N+1 instead of N, then repeat the same way.
% input: two integer
% output: integer
interval(_N, _M) when _M <  _N -> [];
% interval(_N, _M) when _M == _N -> [_N];
interval(_N, _M) when _M >=  _N -> [_N] ++ interval(_N+1,_M).

% Solution: First use interval function get all the element as a list, then sum all the element inside a list.
% advantage: easier t0 write the function, do not need to check if the list is empty
% 	or something else.
% disadvantage: hard to retrive the error.
sum_interval2(_N, _M) -> sum(interval(_N, _M)).



% Check duplicates in the list by adjacent element.
% Solution: separate the list as a head and tail(an individual list), checking if the first head equal the second head or not.
%	if so, put the first head outside as a list, put the tail as a new list repeat.
% input: integer list
% output: integer list
adj_duplicates([]     )               -> [];
adj_duplicates([_]     )               -> [];
adj_duplicates([X1|[X2|Xs]]) when X1 == X2 -> [X1] ++ adj_duplicates([X2|Xs]);
adj_duplicates([_|[X2|Xs]])			   -> adj_duplicates([X2|Xs]).



% Pint out the even element in the list line by line
% Solution: When the head of the list is even, print out with i0:format, store the tail as a new list repeat.
% input: integer list
% output: each integer by one line
even_print(_L    ) when _L == []     -> io:format("");
even_print([X|Xs]) when X rem 2 == 0 -> io:format("~w~n",[X]),even_print(Xs);
even_print([_|Xs])                   -> even_print(Xs).


% Check if the integer is even or odd, and print out the result
% Solution:	
% input: integer
% output: atom(even or odd)
even_odd(_N) ->
	if
		(_N rem 2 == 0) -> even;
		true-> odd
	end.
	

% Pint out the even element in the list line by line.
% Used even_odd function and list comprehensions.
% Solution:
% input: integer list
% output: atom(even or odd)
even_print2(L) -> [io:format("~w~n",[X]) || X <- L, even_odd(X) == even],io:format("").
	


% Get the biggest element for a integer list
% Solution: crate an element to store the biggest number, compare every head 
%	of the list with it, until get the biggest number
% input: integer list
% output: integer
max_check(L)                   -> max_check(L,0).
max_check([],X)            -> X;
max_check([H|T], X) when H > X -> max_check(T,H);
max_check([_|T], X) ->max_check(T,X).

% Divided the list by the biggest element from the same list.
% Solution: Divided every element inside list X by biggest number.
% input: integer list
% output: integer list
normalize([]) -> [];
normalize(L) -> [ X/max_check(L) || X <- L].

% Divided the list by the biggest element from the same list.
% Used: lists:map/2
% input: integer list
% output: integer list
normalize2(L)->lists:map( fun(X) -> X/max_check(L) end,L).


% ---------------------------------------------------------------------- %

% 2. Digitify a number 


% Digitized the integer to single integer in the list.
% Solution: when input number is bigger then 0, running the rest coding.
%	when x div 10 > 0 means x is begger then 10, get the rem 10 by it to store inside the result list.
% input: integer list
% output: integer list
digitize(N) when N>0     -> digitize(N,[]).
digitize(X,R) when (X div 10) > 0  -> digitize((X div 10),[X rem 10]++R);
digitize(X,R) when (X div 10) == 0 -> [X]++R.



% Answer 2
% digitize(X) when N>0 -> digitize(X, []).
% digitize(X, R) when X > 0 -> digitize(X div 10, [X rem 10| R]);
% digitize(X, R) when X == 0 -> R.



% ---------------------------------------------------------------------- %

% 3. Happy numbers (Hint: use the functions digitize and sum).

% Digitize the input integer, then multiply each element by it self and plus
%	together, when the result is 1 means happy.
% Solution:	digitized the input number, if the number is 1 or 4, result show directly.
% 	when is not calculate number by the giving question to get the 
% input: integer
% output: atom
is_happy(N) -> is_happy(N,digitize(N)).
is_happy(X,_) when X == 1 -> true;
is_happy(X,_) when X == 4 -> false;
is_happy(_,R) 			  -> is_happy(sum([E*E||E <- R]), digitize(sum([E*E||E <- R]))).

% Check every integer between two input integer is happy or not, output all the happy number.
% Solution: use interval function to get all the number between two input element, then check all the element with list
%	compression, with the condition is_happy(x)is true.
% input: two integer
% output: integer list
all_happy(_N, _M) -> all_happy(interval(_N, _M)).
all_happy(L) -> [X||X <-L, is_happy(X)==true].

% ---------------------------------------------------------------------- %


% 4. Expressions
% Calculate input atom
% Solution: Used the pattern match to calculate two expression
% input: certain expression string
% output: integer 
expr_eval({mul,Exp1,Exp2})  -> expr_eval(Exp1) * expr_eval(Exp2);
expr_eval({plus,Exp1,Exp2}) -> expr_eval(Exp1) + expr_eval(Exp2);
expr_eval({minus,Exp1,Exp2})-> expr_eval(Exp1) - expr_eval(Exp2);
expr_eval({num,N})          -> N.

% Change from certain pattern sentence till match pattern
% Solution: All the number will change to string by using digitize then append together, by avoid asc|| code by add
% 	every number by 48(since there will be no number bigger then 10)
% input: certain expression string
% output: related math patter 
expr_print({mul,Exp1,Exp2})  -> 
	lists:append(["(",expr_print(Exp1) ,"*" , expr_print(Exp2),")"]);
expr_print({plus,Exp1,Exp2}) -> 
	lists:append(["(",expr_print(Exp1) ,"+" , expr_print(Exp2),")"]);
expr_print({minus,Exp1,Exp2})-> 
	lists:append(["(",expr_print(Exp1) ,"-" , expr_print(Exp2),")"]);
expr_print({num,N}) when N =/=0 -> lists:append([[X+48]||X<-digitize(N)]);
expr_print({num,N}) when N == 0 -> lists:append([[N+48]]).























